import {useState} from 'react';
import Swal from 'sweetalert2';

const AddTask = ({onSave}) => {
    const [text, setText] = useState('');
    const [day, setDay] = useState('');
const onSubmit = (e) => {
    e.preventDefault();
    
    if (!text && !day){
        Swal.fire({
            icon: 'error',
            title: 'Aviso',
            text: 'Por favor, preencha os campos de data e tarefa!',
        })
    } else if (!text && day){
        Swal.fire({
            icon: 'error',
            title: 'Aviso',
            text: 'Por favor, preencha o campo de tarefa!',
        })
    } else if (text && !day){
        Swal.fire({
            icon: 'error',
            title: 'Aviso',
            text: 'Por favor, preencha o campo de data!',
        })
    } else {
        onSave({text, day});
        setText('');
        setDay('');
    }
};
return(
    <form className='add-form' onSubmit={onSubmit}>
        <div className='form-control'>
            <label>Tarefa</label>
            <input type="text" placeholder="Adicionar Tarefa" value={text} onChange={(e) => setText(e.target.value)} />
        </div>
        <div className='form-control'>
            <label>Dia e Hora</label>
            <input type="text" placeholder="Adicionar Dia e Hora" value={day} onChange={(e) => setDay(e.target.value)} />
        </div>
        <input type="submit" value="Salvar Tarefa" className='btn btn-block' />
    </form>
)
}
export default AddTask;
