import Button from "./Button";
import React from "react";
import "../index.css";

const Header = ({ showForm, changeTextAndColor }) => {
    return (
        <header className="header">
            <h2>Lista de Tarefas</h2>
            <Button onClick = {showForm} color={changeTextAndColor ? 'red' : 'green'}
            text={changeTextAndColor ? 'Fechar' : 'Adicionar'} />
        </header>
    )
}
export default Header;
